# Тестовое задание по фронтенд

1.Небходимо сверстать макет

[Ссылка на макет](https://www.figma.com/file/Yi8CRLelsmFUmsOPwAA0Tr/Landing-Page-2?node-id=1%3A26)

Требования:

- готовая страница должна быть адаптивна
- на мобильной версии burger меню
- можно использовать библиотеки/фреймворки(Bootstrap), CSS препроцессоры, инструменты автоматизации(Gulp, Webpack) 
- структура проекта на выходе должна получится

```
 project-name
 - assets
 -- images
 -- css
 index.html
```

2.Реализовать функцию curry чтобы выполнялось:
```javascript
function abc(a, b, c) {
  return a + b + c;
}

function abcdef(a, b, c, d, e, f) {
  return a + b + c + d + e + f;
}

abc.curry('A')('B')('C'); // 'ABC'
abc.curry('A', 'B')('C'); // 'ABC'
abc.curry('A', 'B', 'C'); // 'ABC'

abcdef.curry('A')('B')('C')('D')('E')('F'); // 'ABCDEF'
abcdef.curry('A', 'B', 'C')('D', 'E', 'F'); // 'ABCDEF'
```

3.Что можно улучшить? Как бы вы переписали функцию drawRating при условии что на вход функции drawRating должна приходить переменная vote, содержащая значение от 0 до 100. Интересует именно логика реализации функции, не визуальное отображение звезд.
```javascript
function drawRating(vote) {
	if (vote >= 0 && vote <= 20) {
    	return '★☆☆☆☆';
	}
	else if (vote > 20 && vote <= 40) {
		return '★★☆☆☆';
	}
	else if (vote > 40 && vote <= 60) {
		return '★★★☆☆';
	}
	else if (vote > 60 && vote <= 80) {
		return '★★★★☆';
	}
	else if (vote > 80 && vote <= 100) {
		return '★★★★★';
	}
}

// Проверка работы результата
console.log(drawRating(0) ); // ★☆☆☆☆
console.log(drawRating(1) ); // ★☆☆☆☆
console.log(drawRating(50)); // ★★★☆☆
console.log(drawRating(99)); // ★★★★★
```

Результат выполнения задания необходимо оформить в публичный репозиторий на Github или Bitbucket. В качестве ответа вы присылаете ссылку на ваш репозиторий.